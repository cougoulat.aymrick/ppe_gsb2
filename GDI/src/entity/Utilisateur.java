package entity;

import java.sql.Date;

public class Utilisateur {
	
	int id;
	String nom;
	String prenom;
	String prenom_dieme;
	String prenom_tieme;
	String ville_naissance;
	String adresse;
	String cp;
	String ville;
	String nom_emploi;
	Date date_naissance;
	int id_rang;
	String identifiant;
	String password;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getPrenom_dieme() {
		return prenom_dieme;
	}
	public void setPrenom_dieme(String prenom_dieme) {
		this.prenom_dieme = prenom_dieme;
	}
	
	public String getPrenom_tieme() {
		return prenom_tieme;
	}
	public void setPrenom_tieme(String prenom_tieme) {
		this.prenom_tieme = prenom_tieme;
	}
	
	public String getVille_naissance() {
		return ville_naissance;
	}
	public void setVille_naissance(String ville_naissance) {
		this.ville_naissance = ville_naissance;
	}
	
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	
	public String getNom_emploi() {
		return nom_emploi;
	}
	public void setNom_emploi(String nom_emploi) {
		this.nom_emploi = nom_emploi;
	}
	
	public Date getDate_naissance() {
		return date_naissance;
	}
	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}
	
	public int getId_rang() {
		return id_rang;
	}
	public void setId_rang(int id_rang) {
		this.id_rang = id_rang;
	}
	
	public String getIdentifiant() {
		return identifiant;
	}
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
