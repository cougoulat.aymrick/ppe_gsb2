package gsb.ac.main;

import java.sql.Connection;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class App {

	public static void main(String[] args) {
		Connection cnx = null;
		try {
			System.out.println("coucou");
			cnx = Singleton_connexion.getInstance().getConnexion();
			System.out.println(cnx);
		} catch(ClassNotFoundException | SQLException e) {
			System.out.println("c rate");
			JOptionPane.showMessageDialog(null,
					"Erreur de connexion � la Base",
					"DB erreur : 0x14AFEE15",
					JOptionPane.ERROR_MESSAGE);
			System.exit(1);
			e.printStackTrace();
		}
		LoginDisplay f = new LoginDisplay();
		LoginDAO d = new LoginDAO(cnx);
		new LoginController(f, d);
	}

}
