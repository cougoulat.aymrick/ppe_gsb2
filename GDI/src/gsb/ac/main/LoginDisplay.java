package gsb.ac.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class LoginDisplay extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIdentifiant;
	private JButton btnSeConnecter;
	private JPasswordField textFieldPassword;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginDisplay frame = new LoginDisplay();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginDisplay() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 768);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGestionDeLinventaire = new JLabel("GESTION DE L'INVENTAIRE - GSB");
		lblGestionDeLinventaire.setBounds(168, 53, 646, 47);
		lblGestionDeLinventaire.setFont(new Font("Arial", Font.PLAIN, 40));
		contentPane.add(lblGestionDeLinventaire);
		
		textFieldIdentifiant = new JTextField();
		textFieldIdentifiant.setFont(new Font("Arial", Font.PLAIN, 18));
		textFieldIdentifiant.setBounds(287, 258, 412, 47);
		contentPane.add(textFieldIdentifiant);
		textFieldIdentifiant.setColumns(10);
		
		JLabel lblIdentifiant = new JLabel("Identifiant");
		lblIdentifiant.setFont(new Font("Arial", Font.PLAIN, 26));
		lblIdentifiant.setBounds(287, 211, 124, 36);
		contentPane.add(lblIdentifiant);
		
		JLabel lblMotDePasse = new JLabel("Mot de Passe");
		lblMotDePasse.setFont(new Font("Arial", Font.PLAIN, 26));
		lblMotDePasse.setBounds(287, 377, 276, 36);
		contentPane.add(lblMotDePasse);
		
		btnSeConnecter = new JButton("SE CONNECTER");
		btnSeConnecter.setFont(new Font("Arial", Font.PLAIN, 18));
		btnSeConnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnSeConnecter.setBounds(346, 561, 304, 47);
		contentPane.add(btnSeConnecter);
		
		JLabel lblCompteAdmin = new JLabel("Compte administrateur :");
		lblCompteAdmin.setBounds(10, 654, 172, 14);
		contentPane.add(lblCompteAdmin);
		
		JLabel lblIdentifiantAdmin = new JLabel("Identifiant : admin");
		lblIdentifiantAdmin.setBounds(37, 679, 124, 14);
		contentPane.add(lblIdentifiantAdmin);
		
		JLabel lblMotDePasse_1 = new JLabel("Mot de passe : QVyZoNZocH");
		lblMotDePasse_1.setBounds(37, 704, 200, 14);
		contentPane.add(lblMotDePasse_1);
		
		textFieldPassword = new JPasswordField();
		textFieldPassword.setFont(new Font("Arial", Font.PLAIN, 18));
		textFieldPassword.setBounds(287, 424, 412, 47);
		contentPane.add(textFieldPassword);
	}
	public JTextField getTextFieldIdentifiant() {
		return textFieldIdentifiant;
	}
	public JTextField getTextFieldPassword() {
		return textFieldPassword;
	}
	public JButton getBtnConnexion() {
		return btnSeConnecter;
	}
}
