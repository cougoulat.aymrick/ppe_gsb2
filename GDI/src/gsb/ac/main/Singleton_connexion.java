package gsb.ac.main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Singleton_connexion {
	
	private static Singleton_connexion INSTANCE;
	private Connection cnx;

	private String connection = "jdbc:mysql://192.168.100.5/worlddb?characterEncoding=UTF-8";
	private String db_username = "worlddb_user";
	private String db_password = "123456";
	
	public Singleton_connexion() throws SQLException, ClassNotFoundException {
		this.cnx = DriverManager.getConnection(this.connection, this.db_username, this.db_password);
	}
	
	public static Singleton_connexion getInstance() throws SQLException, ClassNotFoundException {
		if (INSTANCE == null) {
			INSTANCE = new Singleton_connexion();
		}
		return INSTANCE;
	}
	
	public Connection getConnexion() {
		return cnx;
	}
}